$(document).ready(function(){



	function runSlides( slide ) {
        $(slide).addClass('active_slide');
        $('.product img').removeClass('show');
        slideTime = parseInt($(slide).attr('data-time')) * 60000;
        if ( !slideTime ) {
             slideTime = parseFloat($(slide).attr('data-time')) * 60000;
         }
        setTimeout(function(){
            $('.msg').fadeIn(500);
            $('.img').fadeIn(1000);
            $('.img').css('margin', '0');
        }, 2000);
 
        timer = setTimeout(function(){
                $('.img').fadeOut(1000);
                $('.img').css('margin', '100px');
                $('.msg').fadeOut(1000);

                $('.flash').fadeIn(1000);
                $('.flash').addClass('show')
                setTimeout(function(){
                    $('.flash').removeClass('show').fadeOut(1000);
                }, 3000);
                setTimeout(function(){
                    $(slide).removeClass('active_slide');
                    if ( $(slide).next('.slide').length > 0 ) {
                         runSlides($(slide).next('.slide'));
                    }else {
                        runSlides($($('.slide')[0]));
                    }
                }, 2000);
        }, slideTime);
    }

    runSlides($($('.slide')[0]));

});